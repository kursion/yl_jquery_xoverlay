(function($){

  $.fn.extend({ 
    
    
    
    //pass the options variable to the function
    xoverlay: function(options) {
      

      //Set the default values, use comma to separate the settings, example:
      var defaults = {
        zIndexStart : 100,
        filter: [],
        opacity: 0.9,
        border: true,
        borderColor: "#AAAAAA",
        borderStyle: "solid",
        borderSize: 2,
        fontSize1: 12,
        fontSize2: 18,
        fontColor: "#9900a0",
        bgHue: 180
        
        
      }
      var options =  $.extend(defaults, options);
      var xoverlay_zIndex = options.zIndexStart;
      
      this.each(function(index, key) {
        // Get informations of the object
        var width = $(key).width();
        var height = $(key).height();
        var position = $(key).position();
        var id = $(key).attr("id") || "";
        var cl = $(key).attr("class") || "";
        
        if(jQuery.inArray(id, options.filter) == -1){

          var hue = 'rgb(' + (Math.floor((256-199)*Math.random()) + options.bgHue) + ',' + (Math.floor((256-199)*Math.random()) + options.bgHue) + ',' + (Math.floor((256-199)*Math.random()) + options.bgHue) + ')';
        
          // Adding the overlay
          $(key).prepend(""+
          "<div class='xoverlay ' style='"+
            "background-color: "+hue+";"+
            "position: absolute;"+
            "width: "+width+"px;"+
            "height: "+height+"px;"+
            "z-index: "+(xoverlay_zIndex++)+";"+
            "opacity: "+options.opacity+";"+
            "text-align: center;"+
            "font-size: 12px;"+
            "font-family: Arial;"+
            "vertical-align: middle;"+
            "font-weight: 200px"+
          "'>"+
          "<div style='opacity: 1; color: "+options.fontColor+"'>"+
          "id="+id+";class="+cl+":"+width+" x "+height+
          "</div></div>");
        }
      });
      
      
      // Mouse events
      $(".xoverlay").mouseenter(function(e){
        $(this).css("opacity", "0.9");
        $(this).css("font-size", options.fontSize2+"px");
        if(options.border){
          $(this).css("border",
          options.borderColor+" "+
          options.borderSize+"px "+
          options.borderStyle);
        }
        e.stopPropagation();
      });
      $(".xoverlay").mouseleave(function(e){
        $(this).css("opacity", options.opacity);
        $(this).css("font-size", options.fontSize1+"px");
        if(options.border){ $(this).css("border", "none"); }
        e.stopPropagation();
      });
    }
  });
  
})(jQuery); 